INSERT INTO artists (name) VALUES 
("Taylor Swift"), 
("Lady Gaga"),
("Justin Bieber"),
("Ariana Grande"),
("Bruno Mars");
/* 
Taylor Swift - id 3
Lady Gaga - id 4
Justin Bieber - id 5
Ariana Grande - id 6
Bruno Mars - id 7
*/

/* Taylor Swift*/
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Fearless", "2008-01-01", 3);

INSERT INTO songs (song_name, length, genre, album_id) VALUES 
	("Fearless", 246, "Pop rock", 3),
	("Love Story", 213, "Country pop", 3);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Red", "2012-01-01", 3); /* album id - 4*/
INSERT INTO songs (song_name, length, genre, album_id) VALUES 
	("State of Grace", 250, "Rock, alternative rock, arena rock", 4),
	("Red", 204, "Country", 4);

/* Lady Gaga */
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("A Star is Born", "2018-01-01", 4); --5
INSERT INTO songs (song_name, length, genre, album_id) VALUES 
	("Black Eyes", 151, "Rock and roll", 5),
	("Shallow", 201, "Country, rock, folk rock", 5);

-- 6
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Born This Way", "2011-01-01", 4); 
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Born This Way", 252, "Electropop", 6);

-- Justin Bieber (4)
-- album_id 7
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Purpose", "2015-01-01", 5); 
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Sorry", 200, "Dancehall-poptropical housemoombathon", 7);

-----------------------------------

/* ADVANCED SELECTS */

-- Excluding records (!=)
SELECT * FROM songs WHERE id != 1;

-- Greater than, less than, or equal
SELECT * FROM songs WHERE id > 1;
SELECT * FROM songs WHERE id < 9;
SELECT * FROM songs WHERE id = 1;

-- OR
SELECT * FROM songs WHERE id = 1 OR id = 5;

-- IN
SELECT * FROM songs WHERE id IN (1, 3, 5);
SELECT * FROM songs WHERE genre IN ("OPM", "Electropop");

-- Combining Conditions
SELECT * FROM songs WHERE genre = "OPM" AND length > 230; 

-- Find partial matches
-- ENDS WITH
SELECT * FROM songs WHERE song_name LIKE "%a";
-- STARTS WITH
SELECT * FROM songs WHERE song_name LIKE "b%";
-- SELECT keyword in between
SELECT * FROM songs WHERE song_name LIKE "%a%";

SELECT * FROM albums WHERE date_released LIKE "201_-__-__";
-- SELECT keyword in Second Character
SELECT * FROM songs WHERE song_name LIKE "_a%";

-- Sorting 
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY song_name DESC;

-- Distinct - Get all No Duplicates in Genre
SELECT DISTINCT genre from songs;

SELECT * FROM artist
	JOIN albums ON artist.id = albums.artist_id

-- SELECT * FROM ReferenceTable Join TableName ON RefernceTable.PrimaryKey = ReferenceTable.PrimaryKey

SELECT album_title, artist.id FROM artist
	JOIN albums ON artist.id = albums.artist_id;

--  Select columns to be included per table 
SELECT * FROM artist
	JOIN albums ON artist.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;

SELECT song_name from artist
JOIN albums ON artist.id = albums.artist_id
JOIN songs ON albums.id = songs.album_id;

--  Union
SELECT artist.name, albums.album_title FROM artist
	JOIN albums ON artist.id = albums.artist_id;

-- Inner Join
SELECT * FROM artist
	JOIN albums ON artist.id = albums.artist_id

-- Left Join
SELECT * FROM artist
	Left Join albums on artist.id = albums.artist_id;

-- Right Join 
SELECT * FROM albums
	Right Join artist on albums.artist_id = artist.id;